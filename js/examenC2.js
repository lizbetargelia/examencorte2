function change(){
	const mone1 = document.getElementById('selector1');
	const mone2 = document.getElementById('selector2');
	let contenido = "";
	
    if(parseInt(mone1.value) == 1){
        contenido = `<option value="6">Dolar estadounidense</option>
			<option value="7">Dolar canadiense</option>
			<option value="8">Euro</option>`
    }if(parseInt(mone1.value) == 2){
        contenido = `<option value="5">Peso mexicano</option>
			<option value="7">Dolar canadiense</option>
			<option value="8">Euro</option>`
    }if(parseInt(mone1.value) == 3){
        contenido = `<option value="5">Peso mexicano</option>
			<option value="6">Dolar estadounidense</option>
			<option value="8">Euro</option>`
    }if(parseInt(mone1.value) == 4){
        contenido = `<option value="5">Peso mexicano</option>
			<option value="6">Dolar estadounidense</option>
			<option value="7">Dolar canadiense</option>`
    }
	mone2.innerHTML = contenido;
}

function calcular(){
    let valor = document.getElementById('cantidad').value
    let mone1 = document.getElementById('selector1')
    let mone2 = document.getElementById('selector2')
    let subtot = document.getElementById('subtot')
    let comision = document.getElementById('comision')
    let pagar = document.getElementById('pagar')

    //Pesos a
    if(parseInt(mone1.value) == 1 && parseInt(mone2.value) == 6){
        subtot.value = valor / 19.85
    }else if(parseInt(mone1.value) == 1 && parseInt(mone2.value) == 7){
        subtot.value = valor / 14.70
    }else if(parseInt(mone1.value) == 1 && parseInt(mone2.value) == 8){
        subtot.value = valor / 20.05
    }

    //Dolares a
    else if(parseInt(mone1.value) == 2 && parseInt(mone2.value) == 5){
        subtot.value = valor * 19.85
    }else if(parseInt(mone1.value) == 2 && parseInt(mone2.value) == 7){
        subtot.value = valor * 1.35
    }else if(parseInt(mone1.value) == 2 && parseInt(mone2.value) == 8){
        subtot.value = valor * 0.99
    }

    //Dolares canadiences a
    else if(parseInt(mone1.value) == 3 && parseInt(mone2.value) == 5){
        subtot.value =  (valor / 1.35) * 19.85
    }else if(parseInt(mone1.value) == 3 && parseInt(mone2.value) == 6){
        subtot.value = (valor / 1.35)
    }else if(parseInt(mone1.value) == 3 && parseInt(mone2.value) == 8){
        subtot.value =  (valor / 1.35) * 0.99
    }

    //Euro a
    else if(parseInt(mone1.value) == 4 && parseInt(mone2.value) == 5){
        subtot.value = (valor / 0.99) * 19.85
    }else if(parseInt(mone1.value) == 4 && parseInt(mone2.value) == 6){
        subtot.value = (valor / 0.99)
    }else if(parseInt(mone1.value) == 4 && parseInt(mone2.value) == 7){
        subtot.value = (valor / 0.99) * 1.35
    }
    
    comision.value = (subtot.value * .03).toFixed(2)
    pagar.value = (parseFloat(subtot.value) + parseFloat(comision.value)).toFixed(2)
    
    console.log(valor)
    console.log(subtot.value)
    console.log(comision.value)
    console.log(pagar.value)
}

let subtotF = 0
let comisionF = 0
let pagarF = 0

function registrar(){
    let valor = document.getElementById('cantidad')
    let mone1 = document.getElementById('selector1')
    let mone2 = document.getElementById('selector2')
    let subtot = document.getElementById('subtot')
    let comision = document.getElementById('comision')
    let pagar = document.getElementById('pagar')
    let regis = document.getElementById('regis')
    let totales = document.getElementById('totales')
    

    regis.innerText += `Fueron ${valor.value} de la divisa ${mone1.value} a ${mone2.value} con un total ${subtot.value} con una comision ${comision.value} y el total a pagar es ${pagar.value} \n`;

    subtotF += parseFloat(subtot.value)
    comisionF += parseFloat(comision.value)
    pagarF += parseFloat(pagar.value)

    totales.innerText = `${parseFloat(pagarF)}` 
}

function borrar(){
    let regis = document.getElementById('regis')
    let totales = document.getElementById('totales')
	let valor = document.getElementById('cantidad')
    let subtot = document.getElementById('subtot')
    let comision = document.getElementById('comision')
    let pagar = document.getElementById('pagar')

    regis.innerHTML = '';
    totales.innerHTML = '';
    valor.innerHTML = '';
    subtot.innerHTML = '';
    comision.innerHTML = '';
    pagar.innerHTML = '';
    
}